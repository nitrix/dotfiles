<?php
/*                                
========================================
=     |     |    ,---.o|               =
= ,---|,---.|--- |__. .|    ,---.,---. =
= |   ||   ||    |    ||    |---'`---. =
= `---'`---'`---'`    ``---'`---'`---' =
=	      Alex Belanger (nitrix)   =
=	      i.caught.air@gmail.com   =
========================================

MOTIVATION

	 I've never done a very good job of actually maintaining these files.
	 
	 I finally decided that I wanted to be able to execute a single command
	 to "bootstrap" a new system to pull down all of my dotfiles and configs,
	 as well as install all the tools I commonly use.
	 
	 In addition, I wanted to be able to re-execute that command at any time
	 to synchronize anything that might have changed.
	 
	 Finally, I wanted to make it easy to re-integrate changes back in,
	 so that other machines could be updated.

CODE REMINDERS
	
	All paths must be '/' terminated.

*/


//---------------------------------------------
//		   MAIN
//---------------------------------------------

$PATH      = dirname(__FILE__).'/';
$VARIABLES = load_variables();

dotfiles_link($PATH.'link/');
dotfiles_copy($PATH.'copy/');

//---------------------------------------------
//		 FUNCTIONS
//---------------------------------------------

function dotfiles_copy($path, $partial_path='') {
	global $VARIABLES;
	if ($dir = opendir($path)) {
		while (false !== ($entry = readdir($dir))) {
			if ($entry != '.' && $entry != '..') {
				if (is_dir($path.$entry)) {
					dotfiles_copy($path.$entry.'/', $partial_path.$entry.'/');
				} else {
					makepath($partial_path);
					$content = file_get_contents($path.$entry);
					foreach($VARIABLES as $key=>$value) {
						$content = str_replace('{{'.$key.'}}', $value, $content);
					}
					file_put_contents(getenv('HOME').'/.'.$partial_path.$entry, $content);
				}
			}
		}
		closedir($dir);
	}
}

function dotfiles_link($path, $partial_path='') {
	if ($dir = opendir($path)) {
		while (false !== ($entry = readdir($dir))) {
			if ($entry != '.' && $entry != '..') {
				if (is_dir($path.$entry)) {
					dotfiles_link($path.$entry.'/', $partial_path.$entry.'/');
				} else {
					makepath($partial_path);
					if (file_exists(getenv('HOME').'/.'.$partial_path.$entry)) {
						if (sha1_file(getenv('HOME').'/.'.$partial_path.$entry) != sha1_file(getenv('HOME').'/.dotfiles/link/'.$partial_path.$entry)) {
							makepath('dotfiles/backups/'.$partial_path);
							rename(getenv('HOME').'/.'.$partial_path.$entry, getenv('HOME').'/.dotfiles/backups/'.$partial_path.$entry.'.'.time());
							echo '!'."\t".getenv('HOME').'/.'.$partial_path.$entry."\n";
						}
					}
					$success = @symlink($path.$entry, getenv('HOME').'/.'.$partial_path.$entry);
					if ($success) {
						echo '+'."\t".getenv('HOME').'/.'.$partial_path.$entry."\n";
					}
				}
			}
		}
		closedir($dir);
	}
}

function makepath($path) {
	if (!empty($path)) {
		$parts = explode('/',$path);
		for($i=0, $c=count($parts)-1; $i<$c; $i++) {
			$dir = getenv('HOME').'/.';
			for($z=0; $z<=$i; $z++) {
				$dir .= $parts[$z].'/';
			}
			@mkdir($dir);
		}
	}
}

function load_variables() {
	global $PATH;

	$example   = array();
	$variables = array();
	$final     = array();

	$fp = @fopen($PATH.'variables.conf', 'r');
	if ($fp) {
		while (($line = fgets($fp, 4096)) !== false) {
			$line = str_replace("\r", '', str_replace("\n", '', $line));
			$data = explode('=', $line, 2);
			$variables[$data[0]] = $data[1];
		}
		fclose($fp);
	}

	$fp = @fopen($PATH.'variables.conf.example', 'r');
	if ($fp) {
		while (($line = fgets($fp, 4096)) !== false) {
			$line = str_replace("\r", '', str_replace("\n", '', $line));
			$data = explode('=', $line, 2);
			if (@strlen($variables[$data[0]]) == 0) { //cannot use empty because of "0" values.
				//Variable not defined locally, use what the example provides or ask for a value if there's none.
				if (strlen($data[1]) == 0) {
					echo 'Provide a value for variable `'.$data[0].'`: ';
					$stdin = fopen('php://stdin', 'r');
					$value = fgets($stdin, 4096);
					$value = str_replace("\r", '', str_replace("\n", '', $value));
					$final[$data[0]] = $value;
				} else {
					$final[$data[0]] = $data[1];
				}
			} else {
				$final[$data[0]] = $variables[$data[0]];
			}
		}
		fclose($fp);
	} else {
		die('Error: variables.conf.example must not be removed or renamed!'."\n");
	}

	@unlink($PATH.'variables.conf');
	$fp = fopen($PATH.'variables.conf', 'w+');
	foreach($final as $key => $value) {
		fputs($fp, $key.'='.$value."\n");
	}
	fclose($fp);

	return $final;
}
?>
